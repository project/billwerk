<?php

namespace Drupal\billwerk;

use Drupal\subman\Event\SubmanIncomingWebhook;
use Drupal\subman\SubmanSyncInterface;

/**
 * Interface BillwerkSyncInterface.
 */
interface BillwerkSyncInterface extends SubmanSyncInterface {

  /**
   * Returns this implementations title.
   *
   * @return string
   */
  public function getSubscriptionManagementServiceTitle(): string;

  /**
   * Handle an incoming webhook event.
   *
   * @param \Drupal\subman\Event\SubmanIncomingWebhook $event
   *   The event with the data webhook data.
   */
  public function onIncomingWebhook(SubmanIncomingWebhook $event): void;

}
