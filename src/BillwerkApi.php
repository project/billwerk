<?php

namespace Drupal\billwerk;

use Drupal\billwerk\Exception\BillwerkApiErrorException;
use Drupal\Core\Logger\RfcLogLevel;
use GuzzleHttp\Client;
use Drupal\subman\SubmanUtilities;
use Drupal\subman\SubmanEnvironments;
use Drupal\Component\Serialization\Json;

/**
 * Class BillwerkApi.
 *
 * Basic functionality to access the SaaS API of the Billwerk subscription
 * management SaaS.
 *
 * This service class provides the means to actually connect to Billwerk and do
 * thing like:
 * * Authorize against the SaaS api
 * * Find and retrive SaaS data
 * * Send requests to the SaaS api
 * * Handle responses and basic error situations
 *
 * Originally inspired by
 * https://github.com/billwerk/billwerkjs-jquery-php/blob/master/iteroapi.php
 *
 * @todo Refactor re-usable part into an abstract base class in subman.module.
 * @todo Refactor this into a dedicated Drupal-independent composer project?
 * @todo Ensure users may only operate on their own subcriptions!
 */
class BillwerkApi implements BillwerkApiInterface
{
  protected static $urlApiProductionUrl = 'https://app.billwerk.com/';
  protected static $urlApiProductionSelfserviceUrl = 'https://selfservice.billwerk.com/';
  protected static $urlApiSandboxUrl = 'https://sandbox.billwerk.com/';
  protected static $urlApiSandboxSelfserviceUrl = 'https://selfservice.sandbox.billwerk.com/';
  protected static $apiOAuthPath = 'oauth/token';
  protected static $apiBasePath = 'api/v1/';
  protected static $apiContractsResourcePath = 'contracts';
  protected static $apiContractChangesResourcePath = 'contractchanges';
  protected static $apiCustomersResourcePath = 'customers';
  protected static $apiCustomerSearchResourcePath = 'customers';
  protected static $apiDiscountsResourcePath = 'discounts';
  protected static $apiInvoiceDraftsResourcePath = 'invoicedrafts';
  protected static $apiInvoicesResourcePath = 'invoices';
  protected static $apiOrdersResourcePath = 'orders';
  protected static $apiPlanGroupsResourcePath = 'plangroups';
  protected static $apiPlansResourcePath = 'plans';
  protected static $apiPlanVariantsResourcePath = 'planvariants';
  protected static $apiPriceListsResourcePath = 'pricelists';
  protected static $apiProductInfoResourcePath = 'productinfo';
  protected static $apiSubscriptionsResourcePath = 'subscriptions';
  protected static $apiWebhooksResourcePath = 'webhooks';

  /**
   * Amount of seconds the authentication will be cached.
   *
   * @var int
   *   Seconds until cache expiration.
   */
  protected static $authCacheExpiration = 60 * 60;

  const AUTH_PREFIX = 'Authorization: Bearer ';
  const AUTH_CACHE_KEY = 'BillwerkAp::auth_token';
  const RESPONSE_CACHE_NAME = 'BillwerkApi::get::requestCache';
  const SERVICE_URL_KEY_MAIN = 'main';
  const SERVICE_URL_KEY_SELFSERVICE = 'selfservice';

  const SERVICE_ENVIRONMENT_SANDBOX = 'sandbox';
  const SERVICE_ENVIRONMENT_PRODUCTION = 'production';

  /**
   * Constructs a new BillwerkApi object.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The http client.
   * @param \Drupal\subman\SubmanUtilities $utils
   *   The subman utilities.
   * @param \Drupal\subman\SubmanEnvironments $submanEnvironments
   *   The subman environments helper.
   */
  public function __construct(protected readonly Client $httpClient, protected readonly SubmanUtilities $utils, protected readonly SubmanEnvironments $submanEnvironments)
  {
  }

  /**
   * {@inheritDoc}
   */
  public function serviceUrl(string $service = '', string $system = 'main'): string
  {
    $url = $this->utils->getSetting('environment', SubmanEnvironments::ENVIRONMENT_SANDBOX);
    switch ($url) {
      case self::SERVICE_ENVIRONMENT_PRODUCTION:
        $urls = [
          self::SERVICE_URL_KEY_MAIN => self::$urlApiProductionUrl,
          self::SERVICE_URL_KEY_SELFSERVICE => self::$urlApiProductionSelfserviceUrl,
        ];
        break;

      case self::SERVICE_ENVIRONMENT_SANDBOX:
        $urls = [
          self::SERVICE_URL_KEY_MAIN => self::$urlApiSandboxUrl,
          self::SERVICE_URL_KEY_SELFSERVICE => self::$urlApiSandboxSelfserviceUrl,
        ];
        break;

      default:
        throw new BillwerkApiErrorException('Unknown environment: "' . $url . '". Allowed values are "' . SubmanEnvironments::ENVIRONMENT_PRODUCTION . '" or "' . SubmanEnvironments::ENVIRONMENT_SANDBOX . '". Please check your subman.settings!');
    }

    if (!isset($urls[$system])) {
      throw new BillwerkApiErrorException('Service URL could not be determined from URL: "' . $url . '", Service: "' . $service . '"' . ', System: "' . $system . '"');
    }

    return $urls[$system] . $service;
  }

  /**
   * {@inheritDoc}
   */
  public function ensureAuthorization($environment = NULL): void
  {
    if (empty($this->getAuthToken())) {
      if ($environment === NULL) {
        $environment = $this->submanEnvironments->getCurrentEnvironment();
      }
      if (empty($environment)) {
        throw new BillwerkApiErrorException('No environment set.');
      }
      $credentials = $this->utils->getSetting(SubmanEnvironments::SETTINGS_KEY_CREDENTIALS, []);
      $client_id = $credentials[$environment][SubmanEnvironments::SETTINGS_KEY_CLIENT_ID] ?: NULL;
      $client_secret = $credentials[$environment][SubmanEnvironments::SETTINGS_KEY_CLIENT_SECRET] ?: NULL;
      if (empty($client_id)) {
        throw new BillwerkApiErrorException('No Client ID set.');
      }
      if (empty($client_secret)) {
        throw new BillwerkApiErrorException('No Client secret set.');
      }
      $auth = $this->authorize($client_id, $client_secret);
      $this->setAuthToken($auth);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getAuthToken(): ?string
  {
    // Retrieve cached auth token.
    $auth = $this->utils->loadCachedData(self::AUTH_CACHE_KEY);
    return $auth;
  }

  /**
   * {@inheritDoc}
   */
  public function setAuthToken(string $auth): void
  {
    // Cache auth token.
    $this->utils->saveData(self::AUTH_CACHE_KEY, $auth, time() + self::$authCacheExpiration);

    // Log retrieval/caching of new auth token
    // (of course only as excerpt, for security reasons).
    // So we can check how often and when auth token has been retrieved.
    $auth_excerpt = substr($auth, strlen(self::AUTH_PREFIX), 5) . '...' . substr($auth, -6);
    $this->utils->log(
      'setAuthToken(): Authentication token retrieved and cached for @time seconds.',
      ['auth token excerpt' => $auth_excerpt],
      ['@time' => self::$authCacheExpiration],
      'debug'
    );
  }

  /**
   * {@inheritDoc}
   */
  public function authorize(string $clientId, string $clientSecret): string
  {
    // Get oauth token to access SaaS API.
    $verbose = $this->getErrorStream();
    $curl = curl_init();
    $data = http_build_query(['grant_type' => 'client_credentials']);
    // @todo replace curl with Guzzle.
    curl_setopt_array($curl, [
      CURLOPT_URL => $this->serviceUrl(self::$apiOAuthPath),
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      CURLOPT_USERPWD => "$clientId:$clientSecret",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_VERBOSE => TRUE,
      CURLOPT_STDERR => $verbose,
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/x-www-form-urlencoded',
      ],
    ]);
    $response = curl_exec($curl);
    $this->exitOnError($curl, '::authorize', $response);

    // Log (without sensitive information).
    $this->utils->log('authorize(): Authorize', [
      'response size (bytes)' => strlen($response),
    ], [], 'debug');

    // Save auth token.
    $token = json_decode($response);

    return self::AUTH_PREFIX . $token->access_token;
  }

  /**
   * {@inheritDoc}
   */
  public function getContracts(): array
  {
    return $this->get(self::$apiContractsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getContract(string $contractId): array
  {
    return $this->get(self::$apiContractsResourcePath . '/' . $contractId);
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomer(string $customerId): array
  {
    return $this->get(self::$apiCustomersResourcePath . '/' . $customerId);
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomerByEmail(string $email): array
  {
    // @improve as soon as Billwerk offers a search for specific fields, try to use that.
    // (the current search across multiple fields might turn up false positives)
    $results = $this->searchCustomers($email, 1);
    return empty($results) ? NULL : reset($results);
  }

  /**
   * {@inheritDoc}
   */
  public function searchCustomers(string $search, int $limit = NULL): array
  {
    $querystring = "?search=$search";
    if ($limit) {
      $querystring .= '&take=' . $limit;
    }
    return $this->get(self::$apiCustomerSearchResourcePath . '/' . $querystring);
  }

  /**
   * {@inheritDoc}
   */
  public function putCustomer(array $customerData): array
  {
    return $this->put(self::$apiCustomersResourcePath . '/' . $customerData['Id'], $customerData);
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomerContracts(string $customerId): array
  {
    $url = "customers/$customerId/contracts";
    return $this->get($url);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlans(): array
  {
    return $this->get(self::$apiPlansResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlanVariants(): array
  {
    return $this->get(self::$apiPlanVariantsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getSelfserviceTokenArray(string $contractId): array
  {
    return $this->get(self::$apiContractsResourcePath . '/' . $contractId . "/selfServiceToken");
  }

  /**
   * {@inheritDoc}
   */
  public function getUrlSelfservice(string $contractId): ?string
  {
    $response = $this->getSelfserviceTokenArray($contractId);
    return $response['Url'] ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getUrlSignup(string $planVariantId): ?string
  {
    return $this->serviceUrl("portal/signup.html#/$planVariantId", 'selfservice');
  }

  /**
   * Issue a GET request to the API.
   *
   * Use drupal_static temporary static caching of responses during one Drupal
   * execution, to minimize API calls.
   *
   * @param string $resource
   *   The URL of the resource to get.
   *
   * @return mixed
   *   The retrieved data / result.
   */
  private function get(string $resource)
  {
    $cache_key = 'GET:' . $resource;
    $cache = $this->utils->getSetting('cache_get', TRUE);
    $responseCache = &drupal_static(self::RESPONSE_CACHE_NAME, []);
    $result = !$cache ? NULL : ($responseCache[$cache_key] ?? NULL);
    $in_cache = !empty($result);

    // If request is not in cache, perform real request.
    if (!$in_cache) {
      // Make sure we're authorized.
      $this->ensureAuthorization();

      // Assemble request options.
      $token = str_replace('Authorization: Bearer ', '', $this->getAuthToken());
      $guzzle_options = [
        'headers' => [
          'Authorization' => 'Bearer ' . $token,
          'Accept' => 'application/json',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'debug' => FALSE,
        'stream' => FALSE,
      ];

      // Send the request and get the response object.
      $url = $this->serviceUrl(self::$apiBasePath) . $resource;
      $response = $this->httpClient->get($url, $guzzle_options);
      $status = $response->getStatusCode();
      // @todo check whether other staus codes have to be considered ok:
      $status_ok = $status == 200;

      if ($status_ok) {
        // Get json string from response.
        $result = $response->getBody()->getContents();

        // Cache response, if caching requests is enabled.
        if ($cache) {
          $responseCache[$cache_key] = $result;
        }
      }

      // Log.
      $this->utils->log(
        'get(): Sending GET request to @url. Status @status "@reason". Result size in bytes: @size',
        [],
        [
          '@url' => $url,
          '@status' => $status,
          '@reason' => $response->getReasonPhrase(),
          '@size' => strlen($result),
        ],
        $status_ok ? 'debug' : 'error'
      );
    }

    // Convert result from json string to PHP structure.
    $result = Json::decode($result);

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function resetResponseCache(): void
  {
    $responseCache = &drupal_static(self::RESPONSE_CACHE_NAME, []);
    $responseCache = [];
  }

  /**
   * Issue a POST request to the API.
   *
   * @param string $resource
   *   The URL of the resource to POST to.
   * @param mixed $data
   *   The data to post.
   *
   * @return mixed
   *   The result of the operation.
   */
  private function post(string $resource, $data = NULL)
  {
    $this->ensureAuthorization();
    if (is_array($data) && empty($data)) {
      // Billwerk expects objects instead of an empty array!
      $data = new \stdClass();
    }
    $json = json_encode($data);
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/json',
        'Accept: application/json',
        'Content-Length: ' . strlen($json),
        $this->getAuthToken(),
      ],
      CURLOPT_POSTFIELDS => $json,
      CURLOPT_CUSTOMREQUEST => "POST",
    ]);
    $response = $this->request($curl, $resource);
    curl_close($curl);
    return json_decode($response);
  }

  /**
   * Issue a PUT request to the API.
   *
   * @param string $resource
   *   The URL of the resource to put to.
   * @param mixed $data
   *   The data to replace the resource with.
   *
   * @return mixed
   *   The result of the operation.
   */
  private function put(string $resource, $data)
  {
    $this->ensureAuthorization();
    if (is_array($data) && empty($data)) {
      // Billwerk expects objects instead of an empty array!
      $data = new \stdClass();
    }
    $json = json_encode($data);
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_HTTPHEADER => [
        // @todo Remove this? We replaced it by application/json!
        // 'Content-Type: application/x-www-form-urlencoded',
        'Content-Type: application/json',
        'Accept: application/json',
        'Content-Length: ' . strlen($json),
        $this->getAuthToken(),
      ],
      CURLOPT_POSTFIELDS => $json,
      CURLOPT_CUSTOMREQUEST => "PUT",
    ]);
    $response = $this->request($curl, $resource);
    curl_close($curl);
    return json_decode($response);
  }

  /**
   * Issue a DELETE request to the API.
   *
   * @param string $resource
   *   The URL of the resource to put to.
   *
   * @return mixed
   *   The result of the operation.
   */
  private function delete(string $resource)
  {
    $this->ensureAuthorization();
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/json',
        'Accept: application/json',
        $this->getAuthToken(),
      ],
      CURLOPT_CUSTOMREQUEST => 'DELETE',
    ]);
    $response = $this->request($curl, $resource);
    curl_close($curl);
    return json_decode($response);
  }

  /**
   * Execute API request.
   *
   * @param \CurlHandle $handle
   *   The curl object.
   * @param string $resource
   *   The URL of the resource.
   *
   * @return mixed
   *   The result of the operaton.
   */
  private function request(\CurlHandle $handle, string $resource)
  {
    $verbose = $this->getErrorStream();
    curl_setopt_array($handle, [
      CURLOPT_URL => $this->serviceUrl(self::$apiBasePath) . $resource,
      CURLOPT_HTTPAUTH => CURLAUTH_ANY,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_VERBOSE => TRUE,
      CURLOPT_STDERR => $verbose,
    ]);
    $response = curl_exec($handle);
    $this->exitOnError($handle, $resource, $response);
    return $response;
  }

  /**
   * Handles exiting in case http response code is not 200.
   *
   * @param \CurlHandle $handle
   *   The curl handle.
   * @param string $resource
   *   The curl resource.
   */
  private function exitOnError(\CurlHandle $handle, string $resource, bool|string $response)
  {
    $curlError = '';
    $responseInfo = curl_getinfo($handle);
    $httpResponseCode = $responseInfo['http_code'];

    $error_stream = $this->getErrorStream();
    rewind($error_stream);
    $error_stream_contents = stream_get_contents($error_stream, -1, 0);
    if (function_exists('drush_print')) {
      drush_print($error_stream_contents);
    }
    if (!in_array($httpResponseCode, [200, 201, 202], FALSE)) {
      // Response contains details, if not a curl error.
      if (curl_errno($handle)) {
        $curlError = curl_error($handle);
      }
      $this->utils->log('exitOnError(): SaaS service returned error "%response" (%httpstatuscode) on %resource', [
        'curlError' => $curlError,
        'response' => $response,
        'responseInfo' => $responseInfo,
        'output stream' => $error_stream_contents,
      ], [
        '%curlError' => $curlError,
        '%response' => $response,
        '%httpstatuscode' => $httpResponseCode,
        '%resource' => $resource,
      ], 'error');
      throw new BillwerkApiErrorException($response, 0, NULL, [
        'curlError' => $curlError,
        'response' => $response,
        'responseInfo' => $responseInfo,
        'output stream' => $error_stream_contents,
      ]);
    }
  }

  /**
   * Retrieves an error stream to capute and use error output.
   *
   * @return bool|null|resource
   *   The error stream.
   */
  protected function getErrorStream()
  {
    // Originally: return fopen('php://stderr', 'w');.
    $stream = &drupal_static('BillwerkApi::getErrorStream', NULL);
    if ($stream == NULL) {
      $stream = fopen('php://memory', 'r+');
      rewind($stream);
    }
    return $stream;
  }

  /**
   * {@inheritDoc}
   */
  public function getContractChanges(): array
  {
    return $this->get(self::$apiContractChangesResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getContractChange(string $id): array
  {
    return $this->get(self::$apiContractChangesResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomers(): array
  {
    return $this->get(self::$apiCustomersResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoice(string $id): array
  {
    return $this->get(self::$apiInvoicesResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoiceUsage(string $id): array
  {
    return $this->get(self::$apiInvoicesResourcePath . '/' . $id . '/usage');
  }

  /**
   * {@inheritDoc}
   */
  public function getOrders(): array
  {
    return $this->get(self::$apiOrdersResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrder(string $id): array
  {
    return $this->get(self::$apiOrdersResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlanGroups(): array
  {
    return $this->get(self::$apiPlanGroupsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlanGroup(string $id): array
  {
    return $this->get(self::$apiPlanGroupsResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlan(string $id): array
  {
    return $this->get(self::$apiPlansResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getPlanVariant(string $id): array
  {
    return $this->get(self::$apiPlanVariantsResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getPriceLists(): array
  {
    return $this->get(self::$apiPriceListsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getPriceList(string $id): array
  {
    return $this->get(self::$apiPriceListsResourcePath . '/' . $id);
  }

  /**
   * {@inheritDoc}
   */
  public function getWebhooks(): array
  {
    return $this->get(self::$apiWebhooksResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getSubscriptions(): array
  {
    return $this->get(self::$apiSubscriptionsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getProductInfo(): array
  {
    return $this->get(self::$apiProductInfoResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function createOrder(array $data): ?\stdClass
  {
    return $this->post(self::$apiOrdersResourcePath, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function createOrderPreview(array $data): ?\stdClass
  {
    return $this->post(self::$apiOrdersResourcePath, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function commitOrder(string $orderId, array $data = []): ?\stdClass
  {
    return $this->post(self::$apiOrdersResourcePath . '/' . $orderId . '/commit', $data);
  }

  /**
   * {@inheritDoc}
   */
  public function approveOrder(string $orderId): ?\stdClass
  {
    return $this->post(self::$apiOrdersResourcePath . '/' . $orderId . '/approve');
  }

  /**
   * {@inheritDoc}
   */
  public function declineOrder(string $orderId): ?\stdClass
  {
    return $this->post(self::$apiOrdersResourcePath . '/' . $orderId . '/decline');
  }

  /**
   * {@inheritDoc}
   */
  public function deleteOrder(string $orderId): array
  {
    return $this->delete(self::$apiOrdersResourcePath . '/' . $orderId);
  }

  /**
   * {@inheritDoc}
   */
  public function createCustomer(array $customerData): ?\stdClass
  {
    return $this->post(self::$apiCustomersResourcePath, $customerData);
  }

  /**
   * {@inheritDoc}
   */
  public function getContractComponentSubscriptions(string $contractId): array
  {
    return $this->get(self::$apiContractsResourcePath . '/' . $contractId . '/ComponentSubscriptions');
  }

  /**
   * {@inheritDoc}
   */
  public function createContractComponentSubscription(string $contractId, array $data): ?\stdClass
  {
    return $this->post(self::$apiContractsResourcePath . '/' . $contractId . '/ComponentSubscriptions', $data);
  }

  /**
   * {@inheritDoc}
   */
  public function getContractSubscriptions(string $contractId): array
  {
    return $this->get(self::$apiContractsResourcePath . '/' . $contractId . '/Subscriptions');
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoices(): array
  {
    return $this->get(self::$apiInvoicesResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoiceById(string $invoiceId): array
  {
    return $this->get(self::$apiInvoicesResourcePath . '/' . $invoiceId);
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoiceDownloadLink(string $invoiceId): string
  {
    return $this->post(self::$apiInvoicesResourcePath . '/' . $invoiceId . '/downloadLink');
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoiceDrafts(): array
  {
    return $this->get(self::$apiInvoiceDraftsResourcePath);
  }

  /**
   * {@inheritDoc}
   */
  public function getInvoiceDraft(string $invoiceDraftId): array
  {
    return $this->get(self::$apiInvoiceDraftsResourcePath . '/' . $invoiceDraftId);
  }

  /**
   * {@inheritDoc}
   */
  public function createWebhook(array $data): ?\stdClass
  {
    return $this->post(self::$apiWebhooksResourcePath, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function deleteWebhook(string $webhookId): array
  {
    return $this->delete(self::$apiWebhooksResourcePath . '/' . $webhookId);
  }

}
