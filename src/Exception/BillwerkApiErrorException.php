<?php

namespace Drupal\billwerk\Exception;

use Drupal\subman\Exception\SubmanApiErrorException;

/**
 * Billwerk API Exception subtype.
 */
class BillwerkApiErrorException extends SubmanApiErrorException
{
}
