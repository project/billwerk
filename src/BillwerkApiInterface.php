<?php

namespace Drupal\billwerk;

/**
 * Interface BillwerkApiInterface.
 */
interface BillwerkApiInterface {

  /**
   * Returns the SaaS service URL of the SaaS with an optional specific
   * service stub.
   *
   * @param string $service
   *   The additional uri part of the specific service endpoint stub.
   * @param string $system
   *   The additional uri part of the specific service endpoint stub.
   *
   * @return string
   *   The generated SaaS service url.
   */
  public function serviceUrl(string $service = '', string $system = 'main'): string;

  /**
   * Ensures there has been an authentication towards the API.
   */
  public function ensureAuthorization(): void;

  /**
   * Returns the current token for authentication.
   *
   * @return string|null
   *   The current authorization token.
   */
  public function getAuthToken(): ?string;

  /**
   * Stores the given token to be used as current authentication token.
   *
   * For not running into quota limits for auth token retrieval,
   * we store a retrieved auth token in cache for some time (1h by default).
   * Compare https://developer.billwerk.io/Docs/ApiIntroduction#authentication
   * By chosing a cache duration that is less than the SaaS own expiration time,
   * we practically circuvent the need for complex detection of invalid-auth
   * responses, re-authentication and re-sending of questes.
   *
   * @param string $auth
   *   The authoriztation token to be set.
   */
  public function setAuthToken(string $auth): void;

  /**
   * Retrieves auth token from billwerk API.
   *
   * @param string $clientId
   *   The client ID to be used for authentication.
   * @param string $clientSecret
   *   The client secret to be used for authentication.
   *
   * @return string
   *   The authorization token obtained.
   */
  public function authorize(string $clientId, string $clientSecret): string;

  /**
   * Fetch all contracts from API.
   *
   * @return array
   *   The contracts data as array.
   */
  public function getContracts(): array;

  /**
   * Fetch contract by id from API.
   *
   * @param string $contractId
   *   The id of the contract to get.
   *
   * @return array
   *   The contract.
   */
  public function getContract(string $contractId): array;

  /**
   * Returns the contract changes information.
   *
   * @return array
   *   The contract changes.
   */
  public function getContractChanges(): array;

  /**
   * Returns the contract change information.
   *
   * @param string $id
   *
   * @return array
   */
  public function getContractChange(string $id): array;

  /**
   * Returns the customers information.
   *
   * @return array
   */
  public function getCustomers(): array;

  /**
   * Fetch customer by id from API.
   *
   * @param string $customerId
   *   The id of the customer to get.
   *
   * @return array
   *   The customer DTO.
   */
  public function getCustomer(string $customerId): array;

  /**
   * Fetch customer by emaiil address (if existing)
   *
   * @param string $email
   *   The email address of the customer to get.
   *
   * @return array
   *   The customer DTO or NULL if the customer could not be found.
   */
  public function getCustomerByEmail(string $email): array;

  /**
   * Fetch customer from API by a search term.
   *
   * @param string $search
   *   The search term to filter customers on.
   *
   * @return array
   *   The customers data as array.
   */
  public function searchCustomers(string $search, int $limit = NULL): array;

  /**
   * Returns the invoice information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The invoice.
   */
  public function getInvoice(string $id): array;

  /**
   * Returns the invoice change information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getInvoiceUsage(string $id): array;

  /**
   * Update customer in/via API.
   *
   * @param array $customerData
   *   The customer data to update from as array.
   *
   * @return array
   *   The result of the operation.
   */
  public function putCustomer(array $customerData): array;

  /**
   * Gets a list of contracts for given customer id.
   *
   * @param string $customerId
   *   The customer id.
   *
   * @return array
   *   The contracts data as nested array.
   */
  public function getCustomerContracts(string $customerId): array;

  /**
   * Returns the orders information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getOrders(): array;

  /**
   * Returns the order information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getOrder(string $id): array;

  /**
   * Returns the PlanGroups (aka Component) information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPlanGroups(): array;

  /**
   * Returns the PlanGroup (aka Component) information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPlanGroup(string $id): array;

  /**
   * Gets a list of plans.
   *
   * @return array
   *   The plans data as nested array.
   */
  public function getPlans(): array;

  /**
   * Returns the plan information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPlan(string $id): array;

  /**
   * Gets a list of plan variants.
   *
   * @return array
   *   The plan variants data as nested array.
   */
  public function getPlanVariants(): array;

  /**
   * Returns the plan variant information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPlanVariant(string $id): array;

  /**
   * Returns the pricelists information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPriceLists(): array;

  /**
   * Returns the pricelist information.
   *
   * @param string $id
   *   The id.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getPriceList(string $id): array;

  /**
   * Returns the hooks information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getWebhooks(): array;

  /**
   * Returns the subscriptions information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getSubscriptions(): array;

  /**
   * Returns the product information.
   *
   * @return array
   *   The Billwerk API response.
   */
  public function getProductInfo(): array;

  /**
   * Retrieves token for accessing hosted self service pages.
   *
   * @param string $contractId
   *   The contract to get the selfservice token for.
   *
   * @return array
   *   The selfservice token information array retrieved.
   */
  public function getSelfserviceTokenArray(string $contractId): array;

  /**
   * Retrieves the URL for a self-service page for the given contract.
   *
   * @param string $contractId
   *   The contract to get the selfservice url for.
   *
   * @return ?string
   *   The selfservice url retrieved or FALSE.
   */
  public function getUrlSelfservice(string $contractId): ?string;

  /**
   * Retrieves the URL for a signup page for the plan variant id.
   *
   * @param $planVariantId
   *   The ID of the plan variant to get the signup form url for.
   *
   * @return ?string
   *   The retrieved signup form url or FALSE.
   */
  public function getUrlSignup(string $planVariantId): ?string;

  /**
   * Resets temporary the response cache.
   */
  public function resetResponseCache(): void;

  /**
   * Create a new plan/component subscription or up/downgrade order.
   *
   * @param ?\stdClass
   *   OrderDTO.
   */
  public function createOrder(array $data): ?\stdClass;

  /**
   * Create a new plan/component subscription or up/downgrade order preview.
   *
   * For example to show pricing information.
   *
   * @param array $data
   *   OrderDTO.
   *
   * @return ?\stdClass
   *   OrderDTO.
   */
  public function createOrderPreview(array $data): ?\stdClass;

  /**
   * Process and finalize an order.
   *
   * @param string $orderId
   *   Order ID.
   * @param ?\stdClass
   *   OrderCommitDTO.
   */
  public function commitOrder(string $orderId, array $data = []): ?\stdClass;

  /**
   * Approve an order.
   *
   * @param string $orderId
   *   Order ID.
   *
   * @return ?\stdClass
   */
  public function approveOrder(string $orderId): ?\stdClass;

  /**
   * Decline an order.
   *
   * @param string $orderId
   *   Order ID.
   *
   * @return ?\stdClass
   */
  public function declineOrder(string $orderId): ?\stdClass;

   /**
   * Deletes the order.
   *
   * @param string $orderId
   *   Order ID.
   *
   * @return array
   */
  public function deleteOrder(string $orderId): array;

  /**
   * Create customer in/via API.
   *
   * @param array $customerData
   *   The customer data to create from as array.
   *
   * @return ?\stdClass
   */
  public function createCustomer(array $customerData): ?\stdClass;

  /**
   * Retrieves current subscriptions in the selected contract.
   *
   * @param string $contractId
   *   Contract ID.
   *
   * @return array
   */
  public function getContractComponentSubscriptions(string $contractId): array;

  /**
   * Create a new component subscription for this contract.
   *
   * @param string $contractId
   *   Contract ID.
   * @param array $data
   *   ComponentSubscriptionCreateDTO.
   *
   * @return ?\stdClass
   */
  public function createContractComponentSubscription(string $contractId, array $data): ?\stdClass;

  /**
   * Retrieves all currently active subscriptions including plan variant,
   * component subscriptions and discount subscriptions
   *
   * @param string $contractId
   *   Contract ID.
   *
   * @return array
   */
  public function getContractSubscriptions(string $contractId): array;

  /**
   * Retrieves a list of all invoices / credit notes.
   *
   * @return array
   */
  public function getInvoices(): array;

  /**
   * Retrieves an invoice by id.
   *
   * @param string $invoiceId
   *   Invoice ID.
   *
   * @return array
   */
  public function getInvoiceById(string $invoiceId): array;

  /**
   * Creates a file download token for the given invoice.
   *
   * @param string $invoiceId
   *   Invoice ID.
   */
  public function getInvoiceDownloadLink(string $invoiceId): string;

  /**
   * Retrieves a list of all invoice drafts.
   *
   * @return array
   */
  public function getInvoiceDrafts(): array;

  /**
   * Retrieves a draft by id.
   *
   * @param string $invoiceDraftId
   *   Invoice Draft ID.
   *
   * @return array
   */
  public function getInvoiceDraft(string $invoiceDraftId): array;

  /**
   * Create a new webhook subscription.
   *
   * @param array $data
   *   HookDTO.
   *
   * @return ?\stdClass
   */
  public function createWebhook(array $data): ?\stdClass;

  /**
   * Deletes the webhook from the system.
   *
   * @param string $webhookId
   *   Webhook ID.
   *
   * @return array
   */
  public function deleteWebhook(string $webhookId): array;

}
