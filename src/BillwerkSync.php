<?php

namespace Drupal\billwerk;

use Drupal\subman\Event\SubmanIncomingWebhook;
use Drupal\subman\Event\SubmanSynchronousResult;
use Drupal\subman\Exception\SubmanWebhookException;
use Drupal\subman\SubmanSync;
use Drupal\subman\SubmanUtilities;

/**
 * Class BillwerkSync.
 *
 * This is where the generic SubmanSync class is extended for interfacing with the specific subscription management SaaS Billwerk, by implementing some abstract methods and overriding/extending other methods.
 *
 * As a specific implementation of the abstrac SubmanSync class, this class provides:
 * * handling of the webhook event
 * * delegating SaaS api data lookups to class BillwerkApi
 * * normalizing SaaS specific data to a normalized format that
 *   the methods of generic class SubmanSync understands
 *
 * This class is declared as service "subman.sync", and as an event subscriber,
 * using the service tag "event_subscriber".
 * We assume that only one implementation of the abstract class SubmanSync
 * is used at a time. This is an easy way to let other code use a specific
 * service class for a specific SaaS without knowing about it specifically.
 *
 * Any project specific customizations can be done by overriding the
 * "subman.sync" service with a custom class extending this class.
 */
class BillwerkSync extends SubmanSync implements BillwerkSyncInterface
{

  /**
   * Constructs a new BillwerkSync object.
   *
   * @param \Drupal\subman\SubmanUtilities $subman_utilities
   *   The subman utilities service.
   * @param \Drupal\billwerk\BillwerkApi $api
   *   The billwerk api service.
   */
  public function __construct(SubmanUtilities $subman_utilities, protected BillwerkApiInterface $api)
  {
    parent::__construct($subman_utilities);
  }

  /**
   * {@inheritDoc}
   */
  public function getSubscriptionManagementServiceTitle(): string
  {
    return 'Billwerk';
  }

  /**
   * Handle an incoming webhook event.
   *
   * @param \Drupal\subman\Event\SubmanIncomingWebhook $event
   *   The event with the data webhook data.
   */
  public function onIncomingWebhook(SubmanIncomingWebhook $event): void
  {
    // Log event.
    $this->utils->log('onIncomingWebhook():' . $event->getName() . ' for customer_id:' . $event->getSubscriberId() . ', contract_id:' . $event->getSubscriptionId(), $event->getData(), [], 'debug');

    // Process event.
    $this->processIncomingWebhook($event);
  }

  /**
   * {@inheritdoc}
   */
  public function onSynchronousResult(SubmanSynchronousResult $event): void
  {
    $id = $event->request->get('contractId');
    SubmanWebhookException::throwOnEmpty($id, 'Empty contractId.');

    $subscription = $this->getSubscriptionData($id);
    $subscription_type = $this->getSubscriptionType($subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE]);
    $subscription_variant = $this->getSubscriptionType($subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_VARIANT]);

    $this->updateUserByExternalId($subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_SUBSCRIBER_ID]);
    $event->redirectUrl .= '?subscription_type_id=' . $subscription_type[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_ID]
      . '&subscription_type_name=' . $subscription_type[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_NAME]
      . '&subscription_variant_id=' . $subscription_variant[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_ID]
      . '&subscription_variant_name=' . $subscription_variant[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_NAME];
  }

  /**
   * Process an incoming webhook event.
   *
   * For simplicity sake, we always react on events with a full user update,
   * i.e. we always retrieve the subscriber and its subscriptions from the SaaS
   * and store everything in the respective Drupal user.
   *
   * @param SubmanIncomingWebhook $event
   *   The structured event data, including relevant gathered ids in key '_IDs'.
   */
  protected function processIncomingWebhook(SubmanIncomingWebhook $event): void
  {
    // Act on specific events arriving from the subscription management SaaS:
    switch ($event->getName()) {
      case 'Test':
        break;

      // customer_id based events:
      case 'CustomerDeleted':
        $this->deleteUser($event->getSubscriberId(), \Drupal::config('user.settings')->get('cancel_method'));
        break;
      case 'CustomerChanged':
      case 'CustomerCreated':
      case 'CustomerLocked':
      case 'CustomerUnlocked':
      case 'PaymentEscalated':
      case 'PaymentEscalationReset':
        // Other potential events that contain as customer_id:
        // case 'DebitAuthCancelled':
        // Update subscriber:
        $this->updateUserByExternalId($event->getSubscriberId());
        break;

      // contract_id based events:
      case 'ContractCreated':
      case 'ContractChanged':
      // case 'ContractDataChanged':
      case 'ContractCancelled':
        // Other potential events that contain only a contract_id:
        // case 'PaymentBearerExpiring':
        // case 'PaymentBearerExpired':
        // case 'PaymentDataChanged':
        // case 'PaymentProcessStatusChanged':
        // case 'PaymentRegistered':
        // case 'PaymentSucceeded':
        // Get subscription data and subscriber id.
        $subscription = $this->getSubscriptionData($event->getSubscriptionId());
        $subscriber_id = $subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_SUBSCRIBER_ID];
        // Update subscriber.
        $this->updateUserByExternalId($subscriber_id);
        break;

      // Notifications by contract_id.
      case 'TrialEndApproaching':
        // @todo break out this case into a dedicated method.
        $subscription = $this->getSubscriptionData($event->getSubscriptionId());
        $subscriber_data = $this->getSubscriberData($subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_SUBSCRIBER_ID]);
        $user = $this->lookupSubscriber($subscriber_data, FALSE);
        if(!empty($user)){
          $this->notifyUser($user, self::NOTIFY_TRIAL_ENDING);
        }
        break;

      case 'OrderSucceeded':
        // Nothing to do here, as this includes contract and customer creation,
        // which is already handled.
        break;

      case 'RecurringBillingApproaching':
        // We don't handle this in Drupal yet, as it's a typical Billwerk task.
        // If we wanted to inform the user, we could add something like
        // $subscription = $this->getSubscriptionData($event->getSubscriptionId());
        // $subscriber_data = $this->getSubscriberData($subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_SUBSCRIBER_ID]);
        // $user = $this->lookupSubscriber($subscriber_data, FALSE);
        // $this->notifyUser($user, self::NOTIFY_BILLING_APPROACHING);
        break;

      default:
        // Log unhandled events:
        $event->getName();
        $this->utils->log('Event unhandled (you may want to implement the event handling or disable the event at Billwerk Webhooks): ' . $event->getName());
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getNormalizedData(string $type, array $data = NULL): array
  {
    $normalized = parent::getNormalizedData($type, $data);

    if (!empty($data)) {
      if ($type == self::DATA_KEY_SUBSCRIBER) {
        $deleted = !empty($data['DeletedAt']);
        $normalized = array_merge([
          self::MAP_NORMALIZED_SUBSCRIBER_ID => $data['Id'],
          self::MAP_NORMALIZED_SUBSCRIBER_EMAIL => $data['EmailAddress'] ?? '',
          self::MAP_NORMALIZED_SUBSCRIBER_FIRSTNAME => $data['FirstName'] ?? '',
          self::MAP_NORMALIZED_SUBSCRIBER_LASTNAME => $data['LastName'] ?? '',
          self::MAP_NORMALIZED_SUBSCRIBER_DEBITOR_ID => $data['DebitorAccount'] ?? '',
          self::MAP_NORMALIZED_SUBSCRIBER_CREATED => strtotime($data['CreatedAt']),
          self::MAP_NORMALIZED_SUBSCRIBER_ACTIVE => !$data['IsLocked'] && !$deleted,
          self::MAP_NORMALIZED_SUBSCRIBER_DELETED => $deleted,
          self::MAP_NORMALIZED_SUBSCRIBER_LOCALE => $data['Locale'] ?? '',
          self::MAP_NORMALIZED_SUBSCRIBER_LANGUAGE => $data['Language'] ?? '',
        ], $normalized);
      }

      if ($type == self::DATA_KEY_SUBSCRIPTION) {
        // @todo Clarify sensible default for dunning level.
        $escalation_threshold = $this->utils->getSetting('subscription.escalation_revoke_threshold', FALSE);
        $valid_phases = $this->utils->getSetting('subscription.valid_phases', ['Trial', 'Active', 'Normal']);
        $escalation = isset($data['CurrentDunning']) ? $data['CurrentDunning']['Level'] : FALSE;
        $escalation_invalid = ($escalation_threshold === FALSE || $escalation === FALSE) ? FALSE : ($escalation >= $escalation_threshold);
        $valid = !$escalation_invalid && in_array($data['CurrentPhase']['Type'], $valid_phases);
        $normalized = array_merge([
          self::MAP_NORMALIZED_SUBSCRIPTION_ID => $data['Id'],
          self::MAP_NORMALIZED_SUBSCRIPTION_SUBSCRIBER_ID => $data['CustomerId'],
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE => $data['PlanId'] ?? NULL,
          self::MAP_NORMALIZED_SUBSCRIPTION_VARIANT => $data['PlanVariantId'] ?? NULL,
          self::MAP_NORMALIZED_SUBSCRIPTION_REFERENCE => $data['ReferenceCode'],
          self::MAP_NORMALIZED_SUBSCRIPTION_MESSAGE => '',
          self::MAP_NORMALIZED_SUBSCRIPTION_ESCALATION => $escalation,
          self::MAP_NORMALIZED_SUBSCRIPTION_VALID => $valid,
        ], $normalized);
      }

      if ($type == self::DATA_KEY_SUBSCRIPTION_TYPE) {
        $is_plan = isset($data['Name']['_c']);
        $plan_name = $data['Name']['_c'] ?? NULL;
        $description = $data['PlanDescription']['_c'] ?? '';
        $internal_name = $data['InternalName'] ?? '';
        // Used for cosmetic output.
        $variant_name = !$is_plan ? $description . ' (' . $internal_name . ')' : NULL;
        // Used for technical decisionmaking.
        $variant_name_technical = !$is_plan ? $internal_name : NULL;
        $normalized = array_merge([
            // Determine if this is a Plan or PlanVariant:
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_CLASS => $is_plan ? self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_CLASS_MAIN : self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_CLASS_VARIANT,
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_ID => $data['Id'],
            // Get Name for Plan or PlanVariant:
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_NAME => $plan_name ?? $variant_name ?? NULL,
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_NAME_TECHNICAL => $plan_name ?? $variant_name_technical ?? NULL,
          self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_ROLES => $this->utils->explodeValues($data['CustomFields']['cms_roles'] ?? ''),
        ], $normalized);
      }

      if ($type == self::DATA_KEY_ADDONS) {
        $valid_phases = $this->utils->getSetting('subscription.valid_phases', ['Trial', 'Active', 'Normal']);
        $normalized = array_merge([
          self::MAP_NORMALIZED_ADDON_ID => $data['Id'],
          self::MAP_NORMALIZED_ADDON_SUBSCRIBER_ID => $data['CustomerId'],
          self::MAP_NORMALIZED_ADDON_SUBSCRIPTION_ID => $data['ContractId'],
          self::MAP_NORMALIZED_ADDON_QUANTITY => $data['Quantity'],
          self::MAP_NORMALIZED_ADDON_ROLES => $this->utils->explodeValues($data['CustomFields']['cms_roles'] ?? ''),
          self::MAP_NORMALIZED_ADDON_STATUS => $data['Status'],
        ], $normalized);
      }
    }

    return $normalized;
  }

  /**
   * Get the external subscriber data from given key (primary or secondary id).
   *
   * @param string $saasSubscriberId
   *   The key used for external subscriber lookup.
   * @param bool $keyIsSaasPrimaryId
   *   Indicating whether the given $key is the primary id (or the secondary).
   *
   * @return ?array
   *   The subscriber data as nested associative array or NULL if none found.
   */
  protected function lookupUserSaasDataByKey(string $saasSubscriberId, bool $keyIsSaasPrimaryId = TRUE): ?array
  {
    if ($keyIsSaasPrimaryId) {
      return $this->api->getCustomer($saasSubscriberId);
    } else {
      // @improve as soon as Billwerk offers a search for specific fields, try to use that.
      //   (the current search across multiple fields might turn up false positives)
      $results = $this->api->searchCustomers($saasSubscriberId, 1);
      return empty($results) ? NULL : reset($results);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchSubscriberData(string $saasSubscriberId): array
  {
    return $this->api->getCustomer($saasSubscriberId);
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchSubscriptionData(string $saasSubscriptionId): array
  {
    return $this->api->getContract($saasSubscriptionId);
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchSubscriberSubscriptionsData(string $saasSubscriberId): array
  {
    return $this->api->getCustomerContracts($saasSubscriberId);
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchSubscriptionAddonsData(string $saasSubscriptionId): array
  {
    return $this->api->getContractComponentSubscriptions($saasSubscriptionId);
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchSubscriptionTypesData(): array
  {
    return array_merge($this->api->getPlans(), $this->api->getPlanVariants());
  }

  /**
   * {@inheritdoc}
   */
  public function buildEmbedSignup(string $planVariantId, string $mail = NULL): array
  {
    $url = $this->api->getUrlSignup($planVariantId);
    if (!empty($mail)) {
      $url .= $mail ? '?email=' . urlencode($mail) : '';
    }
    return $this->buildEmbed($url);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEmbedSelfservice(string $contractId): array
  {
    $url = $this->api->getUrlSelfservice($contractId);
    return $this->buildEmbed($url);
  }

}
