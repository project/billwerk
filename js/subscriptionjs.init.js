/**
 * @file
 * Billwerk behaviors.
 */
(function (Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.billwerkSubscriptionjsInit = {
    attach(context, settings) {
      if(context === document){
        Drupal.billwerk = {};
        Drupal.billwerk.subscriptionjs = {};

        Drupal.billwerk.subscriptionjs.signupService =
          new SubscriptionJS.Signup();

        // Drupal.billwerk.subscriptionjs.paymentService =
        //   new SubscriptionJS.Payment({
        //     publicApiKey: drupalSettings.billwerk.publicApiKey,
        //   });
      }

    },
  };
})(Drupal, drupalSettings);
