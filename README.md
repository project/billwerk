# Billwerk

TODO

For a full description of the module, visit the
[project page](https://www.drupal.org/project/billwerk).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/billwerk).


## Table of contents (optional)

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers


## Requirements (required)

This module requires the following modules:

- [subman](https://www.drupal.org/project/subman)



## Installation (required, unless a separate INSTALL.md is provided)

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Billwerk configuration

The following steps need to be performed on the Billwerk platform (Sandbox or production):

1. **Setup webhooks at Billwerk (SaaS) > Settings > Integration > Webhooks:**
  1. **For general event handling (required)**
    1. Authentication: *None*
    1. URL: *[Your URL]/subscription-management-service/receive/billwerk/general*
    1. Events (may vary):
      - ContractCreated
      - ContractChanged
      - ContractDataChanged
      - ContractCancelled
      - ContractDeleted
      - CustomerChanged
      - CustomerDeleted
      - DunningCreated
      - InvoiceCreated
      - InvoiceCorrected
      - OrderSucceeded
      - RecurringBillingApproaching
      - TrialEndApproaching
      - CustomerLocked
      - CustomerUnlocked
      - ContractWrittenOff
      - DebitAuthCancelled
      - PaymentBearerExpired
      - PaymentBearerExpiring
      - PaymentDataChanged
      - PaymentEscalated
      - PaymentEscalationReset
      - PaymentFailed
      - PaymentProcessStatusChanged
      - PaymentRegistered
      - PaymentSucceeded
    2. For signup form (optional, only if used)
      1. Authentication: *None*
      1. URL: *[Your URL]/subscription-management-service/synchronous-result/billwerk*
      1. Event: *CustomerCreated*

## Module configuration

1. Enable the module at Administration > Extend.
1. Profit.


## Troubleshooting

TODO!

## FAQ

**Q: TODO?**

**A:** TODO!
