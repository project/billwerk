<?php

namespace Drupal\billwerk_custom_example;

use Drupal\billwerk\BillwerkSync;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\scheduled_executable\Entity\ScheduledExecutable;
use Drupal\subman\Event\SubmanIncomingWebhook;
use Drupal\user\UserInterface;

/**
 * Class BillwerkCustomSync.
 *
 * Provides an alternative project specific class for the subman.sync service
 * class BillwerkSync of the billwerk module.
 *
 * @package Drupal\billwerk_custom_example
 */
class BillwerkCustomSync extends BillwerkSync {

  /**
   * {@inheritdoc}
   */
  public function updateSubscriberFields(UserInterface $subscriber, array $data) {
    parent::updateSubscriberFields($subscriber, $data);
    $this->assignRolesBySubscriptions($subscriber, $data);
    if ($subscriber->hasField('field_number_of_max_subaccounts')) {
      $max_subaccounts = 0;

      // Iterate through all valid subscriptions of the subscriber.
      // Determine the maximum number of subaccounts by the highest number found
      // via the plan variant's InternalName if it starts with "FA" or "SAH".
      foreach ($this->getUserSubscriptions($subscriber) as $subscription) {
        $subscription_variant_id = $subscription[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_VARIANT];
        $subscription_variant = $this->getSubscriptionType($subscription_variant_id);
        if ($subscription_variant) {
          [$plan_name_alpha, $plan_name_number] = sscanf($subscription_variant['InternalName'], "%[A-Z]%d");
          if (in_array($plan_name_alpha, ['FA', 'SAH'])) {
            $max_subaccounts = max($max_subaccounts, $plan_name_number);
          }
        }
      }

      // Set determined number of subaccounts in respective field.
      $subscriber->set('field_number_of_max_subaccounts', $max_subaccounts);
    }

    // For Drupal users newly created by the Billwerk interface...
    if ($subscriber->isNew()) {
      // Assign email address vom Billwerk customer only once when Drupal user is newly created,
      // to allow them to stay independent afterwards.
      // (Default sync of email address is deactivated by removing email from normalized data, via overridden getNormalizedData())
      $subscriber->set('mail', $data[self::DATA_KEY_SUBSCRIBER]['EmailAddress']);

      // Assign "Kundenummer" from Billwerk.
      $subscriber->set('field_customer_number', $data[self::DATA_KEY_SUBSCRIBER][self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIBER_DEBITOR_ID]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNormalizedData(string $type, array $data = NULL): array {
    // Let parent class do default normalization.
    $norm = parent::getNormalizedData($type, $data);

    // Remove email address from normalized data,
    // to allow them to stay independent afterwards.
    // (First assignment for newly created users is done in overridden updateSubscriberFields())
    if ($type == self::DATA_KEY_SUBSCRIBER) {
      unset($norm[self::MAP_NORMALIZED_SUBSCRIBER_EMAIL]);
    }

    return $norm;
  }

  /**
   * {@inheritdoc}
   */
  protected function processIncomingWebhook(SubmanIncomingWebhook $event): void {
    parent::processIncomingWebhook($event);

    $user = $this->getUserBySaasId($event->getSubscriberId());
    switch ($event->getName()) {
      case 'ContractCreated':
        // Schedule a TrialEndApproaching notification being sent to the user:
        // Create an instance of our subman_notify_trial_end_approaching action.
        $actionManager = \Drupal::service('plugin.manager.action');
        $action = $actionManager->createInstance('subman_notify_trial_end_approaching');

        $trial_end_notification_days = $this->utils->getSetting('trial_end_notification_days', FALSE);

        if ($trial_end_notification_days && $user) {
          // Get trial end date of user.
          $trial_end_time = $this->getTrialEndForUser($user);

          // If a trial end was found, create an ScheduledExecutable.
          if ($trial_end_time) {
            $execution_time = max(time(), $trial_end_time - $trial_end_notification_days * 24 * 60 * 60);

            // Create a scheduled_executable with the action and the user.
            $scheduled_executable = ScheduledExecutable::create()
              ->setExecutablePlugin('action', $action)
              ->setTargetEntity($user)
              ->setResolver('default')
              ->setKey('trial_end_approaching:' . $user->id())
              ->setExecutionTime($execution_time);
            $scheduled_executable->save();

            // Log the creation of the scheduled executable.
            $this->utils->log('performAdditionalWebhookAction(): Scheduled trial end mail for subscriber @entity_id on @trial_end_time.', NULL, [
              '@entity_id' => $user->id(),
              '@trial_end_time' => date('Y-m-d H:i:s', $trial_end_time),
            ]);
          }
        }
        break;
    }
  }

  /**
   * Retrieves if/when any trial period ends for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to check for.
   *
   * @return false|int The unix timestamp of the trial end, or FALSE if the user is not in an active uncancelled trial.
   */
  public function getTrialEndForUser(UserInterface $user) {
    $trial_end = FALSE;

    // Only proceed if the user has the role subscriber_trial but _not_ the role license_educational.
    $trial_role = $user->hasRole('subscriber_trial') && !$user->hasRole('license_educational');
    if (!$trial_role) {
      return $trial_end;
    }

    // Check subscriptions of the user for a trial subscription (SPA01).
    $subscriptions = $this->getUserSubscriptions($user, TRUE);
    foreach ($subscriptions as $subscription) {
      $norm = $subscription[self::MAP_NORMALIZED_KEY];

      // Determine whether the subscription is a trial subscription (by its variant's name = SPA01).
      $this->getSubscriptionTypes(TRUE);
      $subscription_type = $this->getSubscriptionType($norm[self::MAP_NORMALIZED_SUBSCRIPTION_VARIANT]);
      $subscription_type_name_trial = $this->utils->getSetting('subscription_name_trial', 'SPA01');
      $subscription_type_name_technical = $subscription_type[self::MAP_NORMALIZED_KEY][self::MAP_NORMALIZED_SUBSCRIPTION_TYPE_NAME_TECHNICAL];
      $is_trial_subscription = ($subscription_type_name_technical == $subscription_type_name_trial);

      // If we have a trial subscription, check whether it's active=uncancelled).
      if ($is_trial_subscription && $subscription['LifecycleStatus'] == 'Active') {
        // Get trial end date from the start date of a second pahse, if there is
        // a second contrac phase defined (which would be a regular plan).
        $phase2_type = $subscription['Phases'][1]['Type'];
        $phase2_start_date = $subscription['Phases'][1]['StartDate'] ?? FALSE;
        if (($phase2_type == 'Normal') && $phase2_start_date) {
          $trial_end = strtotime($phase2_start_date);
        }
      }
    }

    return $trial_end;
  }

  /**
   * Checks whether the given user currently only has a non-terminated trial.
   *
   * @param $user
   *   User The user to check for.
   */
  public function notifyUserOfTrialEnd($user) {
    // Check whether user is active and has still got a trial subscription with not contract end date yet (i.e. not yet terminated).
    if ($user->isActive()) {
      // Optionally get additional subscription information:
      // $sync_data = $this->getSyncDataOfUser($user);
      // Get trial end date of user - if it returns FALSE, the user is not in an active uncanceled trial anymore.
      $trial_end_time = $this->getTrialEndForUser($user);

      // If a trial end was found, send the mail.
      if ($trial_end_time) {
        // Notify the user via email.
        $this->notifyUser($user, self::NOTIFY_TRIAL_ENDING);

        // Log the sending of the email.
        $this->utils->log('notifyUserOfTrialEnd(): Send trial end mail notification to subscriber @entity_id.', NULL, [
          '@entity_id' => $user->id(),
        ]);
      }
    }
  }

  /**
   * Assign the correct roles to the given user according to its subscriptions.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to update the roles on.
   */
  protected function assignRolesBySubscriptions(UserInterface $user, array $data): void
  {
    // First, remove all subman-managed roles from the user.
    $roles_applicable = $this->utils->getSetting('roles_applicable', ['subscriber']);
    foreach ($roles_applicable as $rid) {
      $user->removeRole($rid);
    }

    // Second, assemble all roles according to the user's valid subscriptions'
    // types.
    $roles = [];
    foreach ($this->getUserSubscriptions($user) as $subscription) {
      $subscription_type_id = $subscription[static::MAP_NORMALIZED_KEY][static::MAP_NORMALIZED_SUBSCRIPTION_TYPE];
      $subscription_type = $this->getSubscriptionType($subscription_type_id);
      if ($subscription_type) {
        $subscription_type_roles = (array) $subscription_type[static::MAP_NORMALIZED_KEY][static::MAP_NORMALIZED_SUBSCRIPTION_TYPE_ROLES];
        $roles = array_merge($roles, $subscription_type_roles);
      }
    }

    // Third, assign the user's roles.
    foreach (array_unique($roles) as $rid) {
      if (in_array($rid, $roles_applicable)) {
        $user->addRole($rid);
      }
    }
  }

}
